import { SET_MESSAGE } from '../types/message';

export const apiFetch = (searchResult) => {
  return dispatch => {
    // fetch('').then(resp=>resp.json()).then(res=>console.log('garmi....', res))
    dispatch({
       type: SET_MESSAGE,
       payload: {
        searchResult
       }
    })
  }
}