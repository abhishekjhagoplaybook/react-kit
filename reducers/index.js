import { combineReducers } from 'redux'
import messageReducer from './message'
import search from './search';

const reducers = combineReducers({
    messageReducer,
    search
   })

export default reducers;
