import { SET_MESSAGE, FETCH_SUCCESS } from "../types/message";
const initState = {
  message: ""
};
export default (state = initState, action) => {
  switch (action.type) {
    case SET_MESSAGE:
      console.log(' this is a reducer ', action.payload.searchResult);
      return { ...state, message: action.payload.searchResult };
    default:
      return state;
  }
};
